### LED Options

The important optical parameters of LEDs to consider are the following, one also has to choose between surface mount and through-hole component packaging.

#### Wavelength
Wavelength	470 nm
Spectrum Full Width at Half Maximum (FWHM): This is the same convention used in bandwidth, defined as the frequency range where power drops by less than half (at most −3 dB).

#### Intensity

Lumen	4 Lumen
Millicandela	1200mcd

#### Viewing Angle

Viewing Angle	120 degree

|Brand|Manufacturer No|Wavelength|FWHM|Millicandela|Viewing Angle| Datasheet Link|
|---|---|---|---|---|---|---|
| Superbrightleds.com| [RL5-B12120](https://www.superbrightleds.com/moreinfo/through-hole/5mm-blue-led-120-degree-viewing-angle-flat-tipped-1200-mcd/265/1192/)|470 nm|25 nm|1200mcd|120|N/A|
| TruOpto|[OSB5SA5HA4B-LM](https://www.rapidonline.com/truopto-osb5sa5ha4b-lm-5mm-3-3v-blue-led-oval-diffused-1560mcd-55-2193)|470 nm|25 nm|1200mcd|1560mcd | [Link](https://static.rapidonline.com/pdf/55-2193.pdf)|
	 
### LEDs to Test

 - LED chips e.g. as supplied by https://futureeden.co.uk/
 - LED strips
 - COB LED tape as supplied by https://www.denolighting.com/

To position for 8-well strips, LEDs should be [9mm apart](https://eng.bioneer.com/images/products/Plastic/White8-tubePCRtube03_pop.png) which is not possible using common LED strips.