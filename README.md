# CRISPR-TyphoidDx Reader

## Introduction

Enteric  fever (typhoid and paratyphoid fever) is a systemic infection caused by the gram-negative bacteria Salmonella enterica serovar Typhi (S. Typhi) and Paratyphi  A, B &C (S. Paratyphi) which are highly contagious and rapidly spread through ingestion of contaminated water and food in poor sanitary conditions. Enteric fever in  Cameroon is widely recognized as a national public health concern and Global Health Data Exchange data estimates there were 45,000 cases and 580 deaths in 2016 [Global Burden of diseases, 2017].  In this research, we focus on developing a proof-of-concept rapid nucleic acid test (NAT) for S. Typhi and S. Paratyphi that would address the needs of Cameroonian clinicians, patients and other stakeholders and that could be adapted for rapid genotype based testing of AMR to inform clinical management and allow data collection for AMR surveillance.

In this collaborative work between the Department of Chemical Engineering and Biotechnology, University of Cambridge (CEB), Buea University and Mboalab (Cameroon), we are developing an one-pot nucleic acid test for detection of  S.typhi and S. paratyphi A using Recombinase Polymersae reaction (RPA) and Cas12b. In the presence of RPA amplified target and a guide RNA(gRNA), Cas12 enzyme trans cleaves ssDNA reporter formed of an oligonucelotide flanked by a fluorophore and quencher, resulting in fluorescent signal (Figure.1).  The costs of the tests are maintained low by local manufacturing of enzymes in Cameroon, which we aim to deliver at under 1 GBP per test. 

This repository describes a low-cost detection device that could be easily assembled in Cameroon.

## Design of the CRISPR-TyphoidDx Reader

The device is based on a previous study (Jung et.al., 2020) that could read eight samples (s single PCR strip) in one go. The device consists of assembled 3D printed parts, a simple illuminating system using LEDs for easy visualization of fluorescence in the typhoid test that could be employed in field testing. The FigureXB shows the Version 1.0 of the device and a window for easy visualization of the results. 


### Figure 1 : Simple, low-cost device for fluorescence detection 

#### (A) PCB of the device consisting of 8 LEDs (480nm)
![alt text](Photos/PCB-Photo.png "PCB of the device")

#### (B) an image of the device with 0.2 ml tubes and easily available samples at home: (left to right) red marker pen, water, detergent, diluted detergent, fluorescein eye drop.
![alt text](Photos/Version1/Device-Window.png "PCB of the device")

The device also includes a Raspberry Pi camera for quantification of the fluorescence in point-of-care labs. The camera can be integrated into the visualizing window and slide through each PCR tube to capture the image (Figure 2A).  The testing of the device was carried out by dissolving a red marker pen in water and using a 4£ red optical filter. The RGB image is next thresholded to obtain the average red intensity values in the tube (Figure 2B).

### Figure 2 : Images of a tube containing a red marker pen and a low cost green emission filter, captured using a Raspberry Pi camera. 

#### (A) Captured image from a Raspberry Pi
![alt text](Testing/Version2/position1.jpg "Captured image from a Raspberry P")

#### (B) Image after thresholding for green colour.
![alt text](Testing/Version2/position1.jpg "Image after thresholding for red colour.")

### Figure 3 : Characterizing the variability in total fluorescence intensity at each tube position
![alt text](Testing/Version2/position_calibration.png ) 

Characterization of total fluorescence intensity of the same sample in the eight tube positions shows large variability. This could be the issue in the positioning of the camera while sliding through the samples. A next version of the camera design will incorporate additional controls in the haardware design to reduce this variability. 

## References

Global Burden of Disease Study 2017 (GBD 2017) Data Resources | GHDx. Available online at http://ghdx.healthdata.org/gbd-2017

Jung, J.K., Alam, K.K., Verosloff, M.S. et al. [Cell-free biosensors for rapid detection of water contaminants](https://www.nature.com/articles/s41587-020-0571-7). Nat Biotechnol (2020).



